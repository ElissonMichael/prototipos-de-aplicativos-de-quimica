﻿using UnityEngine;
using System.Collections;

public class Ligação : MonoBehaviour {
	bool selecionado = false;
	bool conectado = false;
	new Renderer renderer;
	public Transform espaço_livre;

	public void conecta_atomo(GameObject outro_atomo){
		Ligação ligação = outro_atomo.GetComponent<Ligação> ();
		transform.parent.transform.position = ligação.posição_livre ();
		transform.parent.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezeAll;
		transform.parent.SetParent(outro_atomo.transform.parent.transform);
		ligação.conectou ();
		Destroy (gameObject);
	}

	public bool disponível(){
		return !conectado;
	}

	public void conectou(){
		conectado = true;
		renderer.material.color = Color.grey;
		Destroy (espaço_livre.gameObject);
		espaço_livre = null;
	}

	public Vector3 posição_livre(){
		return espaço_livre.position;
	}

	public void seleciona(){
		if (selecionado){
			selecionado = false;
			renderer.material.color = Color.white;
		}else if (!conectado){
			selecionado = true;
			renderer.material.color = Color.green;
		}
	}

	void Start () {
		renderer = GetComponent<Renderer> ();
	}
}
