﻿using UnityEngine;
using System.Collections;

public class Distribuição : MonoBehaviour {
	public int[][] camadas = new int[7][];

	void Start () {
		camadas[0] = new int[2]  { 0, 0 };
		camadas[1] = new int[8]  { 0, 0, 0, 0, 0, 0, 0, 0};
		camadas[2] = new int[18] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		camadas[3] = new int[32] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		camadas[4] = new int[32] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		camadas[5] = new int[18] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		camadas[6] = new int[8]  { 0, 0, 0, 0, 0, 0, 0, 0};
	}
	

	public bool Ocupada(int i, int j){
		return camadas[i][j] == 1;
	}

	public bool Desocupada(int i, int j){
		return camadas [i] [j] == 0;
	}

	public void Desocupar(int i, int j){
		camadas[i][j] = 0;
	}

	public void Ocupar(int i, int j){
		camadas[i][j] = 1;
	}

	public bool CamadaCheia(int i){
		bool cheio = true;
		foreach (var j in camadas[i])
			if (j == 0)
				cheio = false;
		return cheio;
	}

	public bool SubnivelCompleto(int camada, string subnivel){
		bool completo = true;
		int inicio, fim = inicio = 0;
		switch (subnivel) {
		case "S":
			inicio = 0;
			fim = 1;
			break;
		case "P":
			inicio = 2;
			fim = 7;
			break;
		case "D":
			inicio = 8;
			fim = 17;
			break;
		case "F":
			inicio = 18;
			fim = 31;
			break;
		default:
			Debug.Log ("Opção de Subnível Inválida!");
			break;
		}
		for (int j = inicio; j <= fim; j++){
			if (camadas [camada] [j] == 0)
				completo = false;
		}
		return completo;
	}

	public string ImprimeCamada(int i){
		string camada_to_s = "";
		foreach (var j in camadas[i])
			camada_to_s = string.Concat(camada_to_s, j.ToString());
		return camada_to_s;
	}

}
