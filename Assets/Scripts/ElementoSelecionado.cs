﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ElementoSelecionado : MonoBehaviour {
	public GameObject[] elementos;
	public Dropdown dropdown;
	public GameObject eletron;
	public GameObject spin_up;
	public GameObject spin_down;
	public Transform destino;
	Elemento elemento;
	int eletrons_restantes = 0;
	TextMesh texto;
	int eletron_atual = 0;
	string[] ordem_correta = new string[118] { "00", "01", 
		"10", "11", "12", "14", "16", "13", "15", "17", 
		"20", "21", "22", "24", "26", "23", "25", "27", "28", "210", "212", "214", "216", "29", "211", "213", "215", "217",
		"30", "31", "32", "34", "36", "33", "35", "37", "38", "310", "312", "314", "316", "39", "311", "313", "315", "317", 
			"318", "320", "322", "324", "326", "328", "330", "319", "321", "323", "325", "327", "329", "331",
		"40", "41", "42", "44", "46", "43", "45", "47", "48", "410", "412", "414", "416", "49", "411", "413", "415", "417", 
			"418", "420", "422", "424", "426", "428", "430", "419", "421", "423", "425", "427", "429", "431",
		"50", "51", "52", "54", "56", "53", "55", "57", "58", "510", "512", "514", "516", "59", "511", "513", "515", "517",
		"60", "61", "62", "64", "66", "63", "65", "67" };

	public void CriaEletron(){
		if (eletrons_restantes > 0) {
			Vector3 posicao_eletron = transform.position;
			posicao_eletron.z -= 0.5f;
			GameObject e = Instantiate (eletron, posicao_eletron, transform.rotation);
			Elétron info_eletron = e.GetComponent<Elétron> ();
			info_eletron.número = eletron_atual;
			info_eletron.destino = ordem_correta [eletron_atual];
			if (int.Parse (info_eletron.destino) % 2 == 0) {
				info_eletron.spin = spin_up;
			} else {
				info_eletron.spin = spin_down;
			}
			eletron_atual += 1;
			eletrons_restantes -= 1;
			texto.text = eletrons_restantes + " Elétrons \n Restantes";
		} else {
			texto.text = "Configuração Eletrônica\n" + elemento.configuracao;
		}
	}

	public void Seleciona(int valor) {
		Instantiate (elementos [valor], destino.position, destino.rotation);
		elemento = elementos [valor].GetComponent<Elemento> ();
		eletrons_restantes = elemento.numero;
		CriaEletron ();
		dropdown.interactable = false;
	}

	void Start () {
		
		texto = gameObject.GetComponentInChildren<TextMesh> ();
		texto.text = "Selecione um \nElemento Acima";
		foreach (GameObject elemento in elementos) {
			Elemento e = elemento.GetComponent<Elemento> ();
			dropdown.options.Add(new Dropdown.OptionData(e.numero + " - " + e.nome + " ("+ e.simbolo +")"));
		}
	}
}
