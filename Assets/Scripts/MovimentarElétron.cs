﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimentarElétron : MonoBehaviour {
	public float z = 0.0f;
	public float limite_x = 7.5f;
	public float limite_y = 4.5f;

	void OnMouseDrag(){
		Vector3 posicao_do_mouse = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, (Camera.main.transform.position.z * -1) + z);
		Vector3 nova_posicao = Camera.main.ScreenToWorldPoint (posicao_do_mouse);
		nova_posicao.x = Mathf.Clamp (nova_posicao.x, -limite_x, limite_x);
		nova_posicao.y = Mathf.Clamp (nova_posicao.y, -limite_y, limite_y);
		transform.position = nova_posicao;
	}
}
