﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Principal : MonoBehaviour {
	public Button botão_para_reiniciar;
	public Button botão_para_sair;
	public Button botão_para_menu;
	void Start(){
		if (botão_para_menu) {
			Button menu = botão_para_menu.GetComponent<Button> ();
			menu.onClick.AddListener (Menu);
		}
		if (botão_para_reiniciar) {
			Button reiniciar = botão_para_reiniciar.GetComponent<Button> ();
			reiniciar.onClick.AddListener (Reiniciar);
		}
		Button sair = botão_para_sair.GetComponent<Button> ();
		sair.onClick.AddListener (Sair);
	}

	void Reiniciar(){
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);
	}

	void Sair(){
		Application.Quit ();
	}

	void Menu(){
		SceneManager.LoadScene (0);
	}

}
