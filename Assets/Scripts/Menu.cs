﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {
	public ToggleGroup toggles;
	public Button botão_para_iniciar;

	void Start () {
		toggles = toggles.GetComponent<ToggleGroup>();
		Button iniciar = botão_para_iniciar.GetComponent<Button> ();
		iniciar.onClick.AddListener (Iniciar);
	}

	void Iniciar () {
		GameObject seleção = toggles.ActiveToggles ().FirstOrDefault ().gameObject;
		if (seleção.CompareTag("Cena_Distribuição")) {
			SceneManager.LoadScene (1);
		} else {
			SceneManager.LoadScene (2);
		}
	}
}
