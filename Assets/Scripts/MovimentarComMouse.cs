﻿using UnityEngine;
using System.Collections;

public class MovimentarComMouse : MonoBehaviour {
	public float velocidade_rotação = 8.0f;
	public float z = 0.0f;
	public bool movimentar_pai = true;
	public float limite_x = 7.5f;
	public float limite_y = 4.5f;

	void OnMouseDrag(){
		if (Ligações.movimento_ativado ()) {
			movimenta ();
		} else {
			rotaciona ();
		}
	}

	void movimenta(){
		Vector3 posicao_do_mouse = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, (Camera.main.transform.position.z * -1) + z);
		Vector3 nova_posicao = Camera.main.ScreenToWorldPoint (posicao_do_mouse);
		nova_posicao.x = Mathf.Clamp (nova_posicao.x, -limite_x, limite_x);
		nova_posicao.y = Mathf.Clamp (nova_posicao.y, -limite_y, limite_y);
		if (transform.parent && movimentar_pai){
			transform.parent.transform.position = nova_posicao;
		}else{
			transform.position = nova_posicao;
		}
	}

	void rotaciona(){
		Vector3 rotação = new Vector3 (Input.GetAxis ("Mouse Y"), 0, Input.GetAxis ("Mouse X")) * velocidade_rotação;
		if (transform.parent){
			transform.parent.transform.Rotate (rotação);
		}else{
			transform.Rotate ( rotação );
		}
	}
}
