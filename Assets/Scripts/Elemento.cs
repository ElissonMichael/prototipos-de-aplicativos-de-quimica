﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elemento : MonoBehaviour {
	public int numero;
	public float peso;
	public string nome;
	public string simbolo;
	public float densidade;
	public int grupo;
	public string configuracao;
}
