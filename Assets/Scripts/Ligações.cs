﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Ligações : MonoBehaviour {
	public Button botão_para_trocar_função;
	GameObject origem;
	public Text texto_no_botão;
	private static bool mover = true;

	public static bool movimento_ativado(){
		return mover;
	}

	void cria_ligação(GameObject objeto_clicado){
		Ligação ligação = objeto_clicado.GetComponent<Ligação> ();
		if (objeto_clicado.tag == "Ligação" && ligação.disponível()) {
			if (origem == null) {
				origem = objeto_clicado;
				ligação.seleciona ();
			} else if (origem == objeto_clicado) {
				origem = null;
				ligação.seleciona ();
			} else {
				origem.GetComponent<Ligação> ().conecta_atomo(objeto_clicado);
				origem = null;
			}
		}
	}

	void ApertouBotão(){
		if (mover){
			mover = false;
			texto_no_botão.text = "Mover";
		}else{
			mover = true;
			texto_no_botão.text = "Rotacionar";
		}
	}

	void Start(){
		Button troca_função = botão_para_trocar_função.GetComponent<Button> ();
		troca_função.onClick.AddListener(ApertouBotão);
	}

	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hit)) {
				cria_ligação (hit.transform.gameObject);
			}
		}
	}
}
