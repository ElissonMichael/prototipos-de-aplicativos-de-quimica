﻿using UnityEngine;
using System.Collections;

public class Espaço : MonoBehaviour {
	new Renderer renderer;
	Collider colisor;
	public int camada_i;
	public int camada_j;
	public Distribuição distribuicao;
	public ElementoSelecionado instanciador;
	Subnivel subnivel;

	void Start () {
		renderer = GetComponent<Renderer> ();
		subnivel = GetComponentInParent<Subnivel> ();
		distribuicao = (Distribuição) FindObjectOfType<Distribuição>();
		instanciador = (ElementoSelecionado) FindObjectOfType<ElementoSelecionado> ();
	}

	string CamadaToS () {
		return camada_i.ToString() + camada_j.ToString();
	}

	void OnTriggerEnter(Collider other) {
		Elétron eletron = other.gameObject.GetComponent<Elétron> ();
		if (CamadaToS() == eletron.destino) {
			renderer.enabled = false;
			Instantiate (eletron.spin, transform.position, eletron.spin.transform.rotation);
			instanciador.CriaEletron ();
			Destroy (other.gameObject);
			distribuicao.Ocupar (camada_i, camada_j);
			if (distribuicao.SubnivelCompleto (camada_i, subnivel.letra)) {
				if (subnivel.proxima_ativacao) subnivel.proxima_ativacao.SetActive (true);
			}
		} else {
			renderer.material.color = Color.red;
		}
	}

	void OnTriggerExit(Collider other) {
		renderer.material.color = Color.white;
	}
}
